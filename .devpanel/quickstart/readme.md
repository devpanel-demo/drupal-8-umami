To create a QuickStart Template of this application, run "create_quickstart.sh"

This will recreate/refresh the following dump files and commit them to your repo:
.devpanel/db.sql.tgz
.devpanel/files.tgz


NOTE: This file is not managed by DevPanel and must be maintained by the author of the each quickstart template.
